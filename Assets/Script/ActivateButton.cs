﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ActivateButton : MonoBehaviour
{
    //　最初にフォーカスするゲームオブジェクト
    [SerializeField]
    public GameObject firstSelect;
    GameObject menuFlagObj;
    private void Start()
    {
        menuFlagObj = GameObject.Find("Main Camera");
    }
    private void Update()
    {
        

        PauseScript pause = menuFlagObj.GetComponent<PauseScript>();

        if(pause.GetMenuFlag())
        {
            ActivateOrNotActivate(true);
            pause.SetMenuFlag();
        }
    }

    public void ActivateOrNotActivate(bool flag)
    {
        for (var i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).GetComponent<Button>().interactable = flag;
        }
        if (flag)
        {
            EventSystem.current.SetSelectedGameObject(firstSelect);
        }
    }

}
