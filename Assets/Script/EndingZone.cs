﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

// Created by Arttu Paldán on 25.11.2020: Basic script for detecting, when player has reached end of the level. 
public class EndingZone : MonoBehaviour
{
    public GameObject finishedGameText;

    public string levelToLoad;

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player")) { finishedGameText.SetActive(true); SceneManager.LoadScene(levelToLoad); }
    }
}
