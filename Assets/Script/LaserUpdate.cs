﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.UIElements;
using System;


public class LaserUpdate : MonoBehaviour
{
    GameObject player;
    ParticleSystem particle;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("robot");

        // Calculate the direction to fly Ray from the direction you are facing
        // 自身が向いている方向からRayを飛ばす方向を算出
        float rad = (transform.localEulerAngles.z + 90) * Mathf.Deg2Rad;
        Vector2 rot = new Vector2(Mathf.Cos(rad), Mathf.Sin(rad));

        particle = GetComponent<ParticleSystem>();
        
        // The first argument is the origin (position) to fly Ray, and the second argument is the direction in which you want to fly Ray.
        // １つ目の引数はRayを飛ばす原点（座標）、２つ目の引数はRayを飛ばしたい方向
        RaycastHit2D hit = Physics2D.Raycast(transform.position, rot);

        if (hit.collider)
        {
            // Adjust particle length   Particleの長さを調整
            float distance = (Vector2.Distance(hit.point, transform.position) - particle.startSize / 2) / particle.startSpeed;

            // ※This is a necessary warning.
            particle.startLifetime = distance;
        }
    }

    private void OnParticleCollision(GameObject other)
    {
        if (other.tag == "Player")
        {
            // Describe the processing when hitting the player  プレイヤーに当たった時の処理を記述
            player.GetComponent<CharCon>().ReStart();

            ParticleSystem pt = GameObject.Find("Laser").GetComponents<ParticleSystem>()[0];
            pt.Pause();
        }

        if (other.tag == "Drone")
        {
            particle.Clear();
        }
    }
}