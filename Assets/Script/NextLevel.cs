﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


//Use this script in a gameobject to make a zone that transports the player into the next level. (to reduce loading times)
//place the gameobject in the midway of the first scene and cut the rest of the map.
//only robot should trigger it
//Scene 0 is "PlayScene" in the folder "Scene", which should the game starts in.
//Scene 1 is "Level1" in the same folder. It should have second half of the map.
//both scenes have the whole map as of right now


public class NextLevel : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        SceneManager.LoadScene(1);
    }



}
