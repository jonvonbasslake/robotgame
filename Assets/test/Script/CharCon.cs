﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharCon : MonoBehaviour
{
   
    [SerializeField]
    private float characterHeightOffset = 0.2f;
    [SerializeField]
    LayerMask groundMask;
    [SerializeField, HideInInspector] Animator animator;
    [SerializeField, HideInInspector]
    SpriteRenderer spriteRenderer;
    [SerializeField, HideInInspector]
    Rigidbody2D rig2d;

    //Flag
    bool swapFlag;


    //Junp
    bool one;

    Vector2 startPos;

    void Awake()
    {
        startPos = gameObject.transform.position;
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        rig2d = GetComponent<Rigidbody2D>();

        swapFlag = true;

        one = true;
    }

    void Update()
    {
        float axis = Input.GetAxis("Horizontal");
        bool isDown = Input.GetAxisRaw("Vertical") < 0;

        Vector2 velocity = rig2d.velocity;

        if (Input.GetKeyDown(KeyCode.Z)) swapFlag = !swapFlag;
        // Move processing
        if (swapFlag == true)
        {
            //JUMP
            if (one && Input.GetButtonDown("Jump"))
            {
                velocity.y = 7;

                one = false;
            }
            //MOVE
            if (axis != 0)
            {
                spriteRenderer.flipX = axis < 0;
                velocity.x = axis * 4;
            }
            rig2d.velocity = velocity;
        }
        //For animation behaviour. When character isn't moving, idle animation playes and when character is moving, movement animation plays?
        if (Input.GetKeyDown("right"))
        {
            animator.SetBool("isMoving", true);
        }
        if (Input.GetKeyDown("left"))
        {
            animator.SetBool("isMoving", true);
        }
        else
        {
            animator.SetBool("isMoving", false);
        }
    }


    void OnCollisionEnter2D(Collision2D other)
    {
        //JUMP Flag On
        one = true;


        if (other.gameObject.tag == "Floor")
        {
            transform.SetParent(other.gameObject.transform);
        }
        if (other.gameObject.tag == "Drone")
        {
            transform.SetParent(other.transform);
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Drone")
        {
            transform.SetParent(collision.transform);
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.tag == "Floor")
        {
            transform.SetParent(null);
        }
        if (other.gameObject.tag == "Drone")
        {
            transform.SetParent(null);
        }
    }

    public void ReStart()
    {
        gameObject.transform.position = startPos;
    }
}
