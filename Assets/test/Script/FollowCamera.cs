﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class FollowCamera : MonoBehaviour
{
    public GameObject player;
    public GameObject Drone;

    //  Flag
    GameObject swapFlag;

    // Use this for initialization
    void Start()
    {
        swapFlag = GameObject.Find("FlagController");
    }

    // Update is called once per frame
    void Update()
    {
        SwapFlag flag = swapFlag.GetComponent<SwapFlag>();
        // Move processing
        if (flag.GetFlag() == false)
            transform.position = new Vector3(Drone.transform.position.x, Drone.transform.position.y, -10);
        else
            transform.position = new Vector3(player.transform.position.x, player.transform.position.y, -10);
    }
}
