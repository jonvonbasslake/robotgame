﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCharacterAction : MonoBehaviour
{
	static int hashSpeed = Animator.StringToHash ("Speed");
	static int hashFallSpeed = Animator.StringToHash ("FallSpeed");
	static int hashGroundDistance = Animator.StringToHash ("GroundDistance");
	static int hashIsCrouch = Animator.StringToHash ("IsCrouch");

	static int hashDamage = Animator.StringToHash ("Damage");

	[SerializeField] private float characterHeightOffset = 0.2f;
	[SerializeField] LayerMask groundMask;

	[SerializeField, HideInInspector] Animator animator;
	[SerializeField, HideInInspector]SpriteRenderer spriteRenderer;
	[SerializeField, HideInInspector]Rigidbody2D rig2d;

    //Flag
    bool swapFlag;

    //Junp
    bool jumpFlag;

    public bool stayAirFlag;


    void Awake ()
	{
		animator = GetComponent<Animator> ();
		spriteRenderer = GetComponent<SpriteRenderer> ();
		rig2d = GetComponent<Rigidbody2D> ();

        swapFlag = true;

        jumpFlag = true;

        stayAirFlag = false;
    }

	void Update ()
	{
		float axis = Input.GetAxis ("Horizontal");
		bool isDown = Input.GetAxisRaw ("Vertical") < 0;


        if (Input.GetKeyDown(KeyCode.Z)) swapFlag = !swapFlag;
        // Move processing
        if (swapFlag == true)
        {

            Vector2 velocity = rig2d.velocity;
            //JUMP
            if (jumpFlag && Input.GetButtonDown("Jump"))
            {
                velocity.y = 7;

                jumpFlag = false;
            }
            //MOVE
            if (axis != 0)
            {
                spriteRenderer.flipX = axis < 0;
                velocity.x = axis * 5;
            }

            rig2d.velocity = velocity;


            var distanceFromGround = Physics2D.Raycast(transform.position, Vector3.down, 1, groundMask);

            // update animator parameters
            animator.SetFloat(hashFallSpeed, rig2d.velocity.y);
            animator.SetFloat(hashSpeed, Mathf.Abs(axis));

            if(stayAirFlag == true)
                animator.SetFloat(hashGroundDistance, distanceFromGround.distance == 0 ? 99 : distanceFromGround.distance - characterHeightOffset);

        }
    }
    

    void OnTriggerEnter2D(Collider2D other)
    {
        animator.SetTrigger(hashDamage);  
    }
    void OnCollisionEnter2D(Collision2D other)
    {
        //JUMP Flag On
        jumpFlag = true;
       
        if (other.gameObject.tag == "Floor")
        {
            transform.SetParent(other.gameObject.transform);
        }
        if (other.gameObject.tag == "Drone")
        {
            transform.SetParent(other.transform);
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Drone")
        {
            transform.SetParent(collision.transform);
        }

        stayAirFlag = false;

    }

    private void OnCollisionExit2D(Collision2D other)
    {
        stayAirFlag = true;

        if (other.gameObject.tag == "Floor")
        {
            transform.SetParent(null);
        }
        if (other.gameObject.tag == "Drone")
        {
            transform.SetParent(null);
        }
    }
}


