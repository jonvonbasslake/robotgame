﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveDrone : MonoBehaviour
{
    // Speed
    public Vector2 SPEED = new Vector2(0.05f, 0.05f);
    //  Flag
    GameObject swapFlag;
    // Use this for initialization

    Rigidbody2D rigid2D;

    bool isGrounded, hasCollidedWithCeiling, hasCollidedWithLeftWall, hasCollidedWithRightWall;

    void Start()
    {
        swapFlag = GameObject.Find("FlagController");
        rigid2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        SwapFlag flag = swapFlag.GetComponent<SwapFlag>();
        // Move processing
        if (flag.GetFlag() == false)
        {
            rigid2D.WakeUp();
            Move();
        }
        else
        {
            rigid2D.Sleep();
        }
    }

    // Movement function
    void Move()
    {
        // Substitute current position for Position
        Vector2 Position = transform.position;
        // Hold down the left key
        if (Input.GetKey("left") && !hasCollidedWithLeftWall)
        {
            // Add and subtract for the assigned Position
            Position.x -= SPEED.x;
        }
        if (Input.GetKey("right") && !hasCollidedWithRightWall)
        { // Hold down the Right key
          // Add and subtract for the assigned Position
            Position.x += SPEED.x;
        }
        if (Input.GetKey("up") && !hasCollidedWithCeiling)
        { // Hold down the Up key
          // Add and subtract for the assigned Position
            Position.y += SPEED.y;
        }
        if (Input.GetKey("down") && !isGrounded)
        { // Hold down the Down key
          // Add and subtract for the assigned Position
            Position.y -= SPEED.y;
        }
        // Substitute the added / subtracted Position at the current position
        transform.position = Position;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Floor")) { isGrounded = true; }
        else if (collision.collider.CompareTag("Ceiling")) { hasCollidedWithCeiling = true; }
        else if (collision.collider.CompareTag("LeftWall")) { hasCollidedWithLeftWall = true;  }
        else if (collision.collider.CompareTag("RightWall")) { hasCollidedWithRightWall = true; }
    }


    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Floor")) { isGrounded = false; }
        else if (collision.collider.CompareTag("Ceiling")) { hasCollidedWithCeiling = false; }
        else if (collision.collider.CompareTag("LeftWall")) { hasCollidedWithLeftWall = false; }
        else if (collision.collider.CompareTag("RightWall")) { hasCollidedWithRightWall = false; }
    }
}
