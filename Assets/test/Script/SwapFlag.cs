﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapFlag : MonoBehaviour
{
    //true=PLayer,false=Drone
    bool moveFlag;

    // Start is called before the first frame update
    void Start()
    {
        moveFlag = true;

    }
    
    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Z))
        {
            moveFlag = !moveFlag;
        }
    }

    public bool GetFlag()
    {
        return moveFlag;
    }
}
